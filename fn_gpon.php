<?php

function gponserver_action ($id, $ch, $connect_url, $connect_api, $action, $export) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $result="";

 curl_setopt($ch, CURLOPT_URL, $connect_url."set_gponserver");

 $pdata = array(
  'id' => $id,
  'action' => $action,
  'export' => $export,
  'api' => $connect_api
 );

 curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

 $return=curl_exec($ch);

 if(curl_errno($ch)) {
  echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
 } else {

  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if ($httpcode == "200") {
   $result_json = json_decode($return, true);
   $result = $result_json["result"];
  } else {
   $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
   echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
  }

 }

 return $result;

}



function gponserver_task ($gponserver, $action, $ch, $connect_url, $connect_api) {
 include ("config.php");
  $func_role = basename(__FILE__)." ".__FUNCTION__ ;

  $data_file=$gpon_dir.$action."_".$gponserver.".data";
  $task_file=$gpon_dir.$action."_".$gponserver.".task";

  if (!file_exists($task_file)) {

   curl_setopt($ch, CURLOPT_URL, $connect_url."get_gponserver_task");

   $pdata = array(
    'gponserver' => $gponserver,
    'action' => $action,
    'api' => $connect_api
   );

   curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

   $return=curl_exec($ch);

   if(curl_errno($ch)) {
    echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
   } else {

    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpcode == "200") {

     $config_data = json_decode($return,true);
     $fp = fopen($data_file, "w");
     fwrite($fp, json_encode($config_data));
     fclose($fp);

     $execute_name=$driver_dir."gen_".$action.".sh";
     $execute_command=$execute_name." ".$gponserver." < /dev/null >> ".$log_dir."vivabill.log &";
     $return=system_exec($execute_command);

     if ( $return != "0" ) {
      echo system_addlog($func_role, $return, $execute_command);
     }

    } else {
     $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
     echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
    }

   }

  } else {
   echo message_addlog($func_role,"Task ".$action."_".$gponserver." is exists");
  }

}



function gponserver_check ($action, $ch, $connect_url, $connect_api) {
 include("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 foreach (glob($gpon_dir.$action."*.task") as $filename) {
  $data = file_get_contents ($filename);
  $config_json = json_decode($data, true);

  $id=$config_json["process"]["id"];
  $pid=$config_json["process"]["pid"];
  $status=$config_json["process"]["status"];

  $file_task=$gpon_dir.$action."_".$id.".task";
  $file_data=$gpon_dir.$action."_".$id.".data";

  if ( $status == "run") {

  }


  if ( $status == "success") {

   $export=$config_json["process"]["export"];
   $arr_export=explode("~ent~",$export);
   $cou_export = count($arr_export) - 1;

   if ( gponserver_action($id, $ch, $connect_url, $connect_api, $action, $export) == "success") {
    if ( $cou_export > 0 ) {
     echo system_addlog($func_role, "0", "Task ".$action." ID-".$id." - changed ".$cou_export." rows");
    }
   } else {
    echo system_addlog($func_role, "1", "Task ".$action." ID-".$id." - error");
   }

   if (file_exists($file_task)) { unlink($file_task); }
   if (file_exists($file_data)) { unlink($file_data); }

  }


  if ( $status == "error") {

   echo system_addlog($func_role, "1", "Task ".$action." ID-".$id." - error");
   if (file_exists($file_task)) { unlink($file_task); }
   if (file_exists($file_data)) { unlink($file_data); }

  }


 }

}



function gponserver_config($a_gponserver,$ch,$connect_url,$connect_api) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 $R="\033[0;31m"; $G="\033[0;32m"; $N="\033[0m";
 $a_proc = array("onu", "vlan", "servis", "serial");

 foreach ($a_gponserver as $chassis) {
  foreach ($a_proc as $process) {

   $fh=$gpon_dir."oltg_".$process."_".$chassis.".exp";
   $fl=$gpon_dir."oltg_".$process."_".$chassis.".log";
   $ff=$log_dir."gpon_chassis_".$chassis.".log";

   curl_setopt($ch, CURLOPT_URL, $connect_url."get_gpon_status");

   $pdata = array(
    'process' => $process,
    'chassis' => $chassis,
    'gpon_dir' => $gpon_dir,
    'api' => $connect_api
   );

   curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

   $return=curl_exec($ch);

   if(curl_errno($ch)) {
    echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
   } else {

    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpcode == "200") {

     $config = json_decode($return,true);
     $action=$config["action"];

     if ( $action == "1" ) {

      if (!file_exists($fl)) {
       if (file_exists($fh)) { unlink($fh); }
       $arr_expect=$config["expect"];
       if ( count($arr_expect) >0 ) {
        foreach ($arr_expect as $row) {
         file_put_contents($fh , $row.chr(10), FILE_APPEND | LOCK_EX);
        }
       }
       file_put_contents($ff, chr(10)."${G}[".date('Y-m-d H:i:s', time())."] [".$func_role."] [".$process."]${N}".chr(10), FILE_APPEND | LOCK_EX);
       echo system_exec_addlog ($func_role, $expect." -f ".$fh." >> ".$ff);
       if (file_exists($fh)) { unlink($fh); }
      }

      register_parse($fl,$ff, $process, $chassis, $ch, $connect_url, $connect_api);

     }

    } else {
     $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
     echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
    }

   }

  }
 }

}


function register_parse($fl,$ff, $process, $chassis, $ch, $connect_url, $connect_api) {
 include("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $R="\033[0;31m"; $G="\033[0;32m"; $N="\033[0m";

 if (file_exists($fl)) {

  $content = file_get_contents($fl);
  $content = str_replace(chr(10),"~e~",$content);
  $content = str_replace(chr(13),"~t~",$content);
  $content = str_replace(",","~m~",$content);
  $content = str_replace(":","~l~",$content);
  $content = str_replace("\"","~b~",$content);
  $content = str_replace("{","~fr~",$content);
  $content = str_replace("}","~fl~",$content);
  $content = str_replace("/","~s~",$content);
  $content = preg_replace( "/[^[:print:]]/", "",$content);
  $content = json_encode($content);
  $content = str_replace(" [1D","",$content);


  curl_setopt($ch, CURLOPT_URL, $connect_url."set_gpon");

  $pdata = array(
   'process' => $process,
   'chassis' => $chassis,
   'content' => $content,
   'api' => $connect_api
  );

  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($pdata));

  $return=curl_exec($ch);
  if(curl_errno($ch)) {
   echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
  } else {

   $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   if ($httpcode == "200") {

    $config = json_decode($return,true);
    $status=$config["status"];

    if ( $status == "success" ) {
     echo system_addlog($func_role, "0", "Success configure [".$process."] for chassis [".$chassis."]" );
     file_put_contents($ff, chr(10)."${G}[SUCCESS]${N}".chr(10), FILE_APPEND | LOCK_EX);
     if (file_exists($fl)) { unlink($fl); }
    } else {
     echo system_addlog($func_role, "1", "Error configure [".$process."] for chassis [".$chassis."]" );
     echo system_exec_addlog ($func_role, $cat." ".$fl." >> ".$ff);
     file_put_contents($ff, chr(10)."${R}[ERROR]${N}".chr(10), FILE_APPEND | LOCK_EX);
     unlink($fl);
    }

   } else {
    $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
   }

  }

 }

}

?>
