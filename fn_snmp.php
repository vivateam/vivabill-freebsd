<?php


function trapserver_alarm_action($id_trapserver, $ch, $connect_url, $connect_api, $date_alarm, $ip_alarm, $port_alarm, $status_alarm) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $result="";

 curl_setopt($ch, CURLOPT_URL, $connect_url."set_trapserver_alarm");

 $pdata = array(
  'id_trapserver' => $id_trapserver,
  'date_alarm' => $date_alarm,
  'ip_alarm' => $ip_alarm,
  'port_alarm' => $port_alarm,
  'status_alarm' => $status_alarm,
  'api' => $connect_api
 );

 curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

 $return=curl_exec($ch);

 if(curl_errno($ch)) {
  echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
 } else {

  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if ($httpcode == "200") {
   $result_json = json_decode($return, true);
   $result = $result_json["result"];
  } else {
   $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
   echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
  }

 }

 return $result;

}


function trapserver_check ($id_trapserver, $ch, $connect_url, $connect_api) {
 include("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $traps_list = glob($traps_dir."*.task");

 foreach($traps_list as $file_task) {

  $data_task = file_get_contents ($file_task);
  $config = json_decode($data_task, true);

  $head=$config["head"];

  if ( $head == "alarm" ) {

   $date_alarm=$config["date"];
   $ip_alarm=$config["ip"];
   $port_alarm=$config["port"];
   $status_alarm=$config["status"];

   if ( trapserver_alarm_action($id_trapserver,  $ch, $connect_url, $connect_api, $date_alarm, $ip_alarm, $port_alarm, $status_alarm) == "success") {
    if (file_exists($file_task)) { unlink($file_task); }
    echo system_addlog($func_role, "0", "ALARM date-".$date_alarm." ip-".$ip_alarm." port-".$port_alarm." status-".$status_alarm);
   }

  }

  if ( $head == "unknown" ) {
   if (file_exists($file_task)) { unlink($file_task); }
  }

 }

}

function trapserver_syncro ($id_trapserver, $ch, $connect_url, $connect_api) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 $snmpd_config=$snmp_dir."snmptrapd.conf";

 curl_setopt($ch, CURLOPT_URL, $connect_url."get_trapserver");

 $pdata = array(
  'id_trapserver' => $id_trapserver,
  'api' => $connect_api
 );

 curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

 $return=curl_exec($ch);

 if(curl_errno($ch)) {
  echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
 } else {
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if ($httpcode == "200") {

   $result_json = json_decode($return, true);
   $trap_active=$result_json["trap_active"];

   if  ( $trap_active == "1" ) {

    if (file_exists($snmpd_config)) { unlink($snmpd_config); }

    $trap_community=$result_json["trap_community"];
    file_put_contents($snmpd_config, "authCommunity log,execute,net ".$trap_community.chr(10), FILE_APPEND | LOCK_EX);
    file_put_contents($snmpd_config, "traphandle default ".$driver_dir."gen_snmptrap.sh".chr(10), FILE_APPEND | LOCK_EX);
    echo system_exec_addlog ($func_role, $snmptrapd." restart >/dev/null");

   }

  } else {
   $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
   echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
  }

 }

}

?>
