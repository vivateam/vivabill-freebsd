<?php

 function task_manager($vcpid) {

 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 include("config.php");
 include("fn_core.php");
 include("fn_puppet.php");
 include("fn_ipfw.php");
 include("fn_dhcp.php");
 include("fn_vrdat.php");
 include("fn_tftp.php");
 include("fn_ovsp.php");
 include("fn_comx.php");
 include("fn_gpon.php");
 include("fn_alarm.php");
 include("fn_snmp.php");
 include("fn_cmts.php");
 include("fn_stat.php");
 include("fn_errors.php");

 $pid_file=$pid_dir."vivabill.pid";

 if (file_exists($pid_file)) {

  // b_work -------------------------------------------------

  $a_vfpid=file($pid_file);
  $vfpid=$a_vfpid[0];

  if ( $vfpid == $vcpid ) {

  if (!file_exists($data_dir)) { mkdir($data_dir, 0777, true); }
  if (!file_exists($driver_dir)) { mkdir($driver_dir, 0777, true); }
  if (!file_exists($comx_dir)) { mkdir($comx_dir, 0777, true); }
  if (!file_exists($certs_dir)) { mkdir($certs_dir, 0777, true); }
  if (!file_exists($ovsp_dir)) { mkdir($ovsp_dir, 0777, true); }
  if (!file_exists($stat_dir)) { mkdir($stat_dir, 0777, true); }
  if (!file_exists($log_dir)) { mkdir($log_dir, 0777, true); }
  if (!file_exists($cmts_dir)) { mkdir($cmts_dir, 0777, true); }
  if (!file_exists($gpon_dir)) { mkdir($gpon_dir, 0777, true); }
  if (!file_exists($alarm_dir)) { mkdir($alarm_dir, 0777, true); }
  if (!file_exists($traps_dir)) { mkdir($traps_dir, 0777, true); }
  if (!file_exists($errors_dir)) { mkdir($errors_dir, 0777, true); }

   exec($uname." -r | cut -d '.' -f1",$rname_vers);
   exec($uname." -o",$rname_os);
   exec($uname." -r",$rname_rel);
   exec($hostname, $rname_host);

   $name_vers=$rname_vers[0];
   $name_os=$rname_os[0];
   $name_rel=$rname_rel[0];
   $name_host=$rname_host[0];

   $data = file_get_contents ($vivapi);
   $config = json_decode($data, true);
   $uuid=$config["uuid"];
   $connect_url=$config["connect"]["url"]."/viva/";
   $connect_api=$config["connect"]["api"];
   $connect_username=$config["connect"]["username"];
   $connect_password=$config["connect"]["password"];

   echo message_addlog ($func_role, "Open process UUID=".$uuid);

   $rproc=true;
   $current_maintain = "0";
   $current_notify_interval = 10;
   $counter = 0;
   $first_cycle = "1"; 

   // Set SSL parameters
   $ch = curl_init();
   $headers = array("Content-Type: application/json");
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
   curl_setopt($ch, CURLOPT_CAINFO, $certs_dir."viva-cert.pem");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_USERPWD, $connect_username . ":" . $connect_password);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

   //Set HW
   curl_setopt($ch, CURLOPT_URL, $connect_url."set_hw");

   $pdata = array(
    'uuid' => $uuid,
    'name_os' => $name_os,
    'name_rel' => $name_rel,
    'hostname' => $name_host,
    'api' => $connect_api
   );

   curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

   $return=curl_exec($ch);
   if(curl_errno($ch)) {
    echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
   } else {
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpcode == "200") {

     $status_load = json_decode($return,true);
     $status=$status_load["status"];
     $message=$status_load["message"];
     echo system_addlog($func_role,$status,$message);

    } else {
     $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
     echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
    }
   }

   echo message_addlog ($func_role, "Start Task manager with API v.".$connect_api." URL ".$connect_url);

   while ($rproc == true) { // Start cycle

    if (!file_exists($pid_file)) {
     echo message_addlog ($func_role, "Stop Task manager");
     $rproc=false;
     break;
    } else {
     $a_vfpid=file($pid_file);
     $vfpid=$a_vfpid[0];
     if ( $vfpid != $vcpid ) {
      echo message_addlog ($func_role, "Lost process UUID=".$uuid);
      $rproc=false;
      break;
     }
    }

   // COUTNER
   if ($counter>600) {
    $counter=0;
   }


     //#######################
    //# VI_CRON (crontab)   #
   //#######################
   $vi_cron = array();
   foreach (glob($data_dir."*.cron") as $file_cron) {
    $data = file_get_contents ($file_cron);
    $config_cron = json_decode($data, true);
    $period=$config_cron["process"]["period"];
    $ddrt=$config_cron["process"]["ddrt"];
    $ddr=$config_cron["process"]["ddr"];
    $ddrtm=$config_cron["process"]["ddrtm"];
    $vi_cp = array("ddrt" => $ddrt, "ddr" => $ddr, "ddrtm" => $ddrtm);
    $vi_cron[$period] = $vi_cp;
    if (file_exists($file_cron)) { unlink($file_cron); }
   }


   //#######################
   //# START HW SEMAPHORES #
   //#######################

   curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_hw");

   $pdata = array(
    'uuid' => $uuid,
    'api' => $connect_api
   );

   curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

   $return=curl_exec($ch);

   if(curl_errno($ch))
   {
    echo  system_addlog($func_role,"1","URL ".$current_url." error: ". curl_error($ch) );
   } else {

    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($httpcode == "200") {

     $hw = json_decode($return, true);

     // Get TASK options
     $sm_maintain=$hw["maintain"];
     $sm_notify_interval=$hw["notify_interval"];

     // get PROC options
     $router=$hw["router"];
     $sm_vpnserver=$hw["vpnserver"];
     $dhcpserver=$hw["dhcpserver"];
     $sm_dhcprelay=$hw["dhcprelay"];
     $sm_radserver=$hw["radserver"];
     $tftpserver=$hw["tftpserver"];
     $sm_bmrserver=$hw["bmrserver"];
     $sm_iptvserver=$hw["iptvserver"];
     $gponserver=$hw["gponserver"];
     $sm_eponserver=$hw["eponserver"];
     $cmtsserver=$hw["cmtsserver"];
     $statistic=$hw["statistic"];
     $sm_brasserver=$hw["bras"];
     $cmanager=$hw["cmanager"];
     $trapserver=$hw["trapserver"];

     // get maintain data
     if ( $current_maintain != $sm_maintain ) {
     switch ($sm_maintain) {
      case "0":
       echo message_addlog ($func_role, "End of maintain");
       break;
      case "1":
       echo message_addlog ($func_role, "Begin maintain");
       break;
      }
     }

     // get notify data
     if ( $current_notify_interval != $sm_notify_interval ) {
      echo message_addlog ($func_role, "Interval queries to API set to ".$sm_notify_interval." second");
     }

     $current_maintain=$sm_maintain;
     $current_notify_interval=$sm_notify_interval;

     if ( $sm_maintain == "0" ) { // begin task process

        ////////////////////////
       // PUPPET declaration //
      ////////////////////////
      check_roles_facter ($hw,$data_dir);

        ////////////////////////
       // TFTP Server        //
      ////////////////////////
      if ( $tftpserver != "none" ) {

       curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_tftpserver");

       $pdata = array(
        'id_tftp_server' => $tftpserver,
        'api' => $connect_api
       );

       curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

       $return=curl_exec($ch);

       if(curl_errno($ch)) {
        echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
       } else {
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == "200") {

         $tftp = json_decode($return, true);
         $sm_tftp_server_config=$tftp["config"];
         $sm_tftp_server_modem=$tftp["modem"];
         $sm_tftp_server_check=$tftp["check"];

         if ( $sm_tftp_server_config=="1" ) {
          echo message_addlog($func_role, "[tftp_server_config]");
          tftp_server_config($tftpserver, $ch, $connect_url, $connect_api);
         }

         if ( $sm_tftp_server_modem=="1" ) {
          echo message_addlog($func_role, "[tftp_server_modem]");
          tftp_server_modem($tftpserver, $ch, $connect_url, $connect_api);
         }

         if ( $sm_tftp_server_check=="1" ) {
          echo message_addlog($func_role, "[tftp_server_check]");
          tftp_server_check($tftpserver, $ch, $connect_url, $connect_api);
         }

         tftp_server_task_result($tftpserver, $ch, $connect_url, $connect_api);

        } else {
         $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
         echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
        }
       }

      }

        ////////////////////////
       // DHCP Server        //
      ////////////////////////
      if ( $dhcpserver != "none" ) {

       curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_dhcpserver");

       $pdata = array(
        'id_dhcp_server' => $dhcpserver,
        'api' => $connect_api
       );

       curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

       $return=curl_exec($ch);

       if(curl_errno($ch)) {
        echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
       } else {
         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         if ($httpcode == "200") {

          $dhcp = json_decode($return, true);
          $dhcpserver_config=$dhcp["config"];
          $dhcpserver_update=$dhcp["update"];

          if ( $dhcpserver_config=="1" ) {
           echo message_addlog($func_role, "[dhcpserver_config]");
           dhcpserver_config($dhcpserver, $ch, $connect_url, $connect_api);
          }

          if ( $dhcpserver_update=="1" ) {
           echo message_addlog($func_role, "[dhcpserver_update]");
           dhcpserver_update($dhcpserver, $ch, $connect_url, $connect_api);
          }


         } else {
          $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
         }

       }

      }


        ////////////////////////
       // Trapserver         //
      ////////////////////////

      if ( $trapserver != "none" ) {

       curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_trapserver");

       $pdata = array(
        'id_trapserver' => $trapserver,
        'api' => $connect_api
       );

       curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

       $return=curl_exec($ch);

       if(curl_errno($ch)) {
        echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
       } else {

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == "200") {

         $rou = json_decode($return, true);

         $trapserver_active=$rou["trapserver"]["active"];
         $trapserver_syncro=$rou["trapserver"]["syncro"];

         if ( ( $trapserver_active == "1" && $trapserver_syncro == "1" ) || $first_cycle == "1" ) {
          echo message_addlog($func_role, "[trapserver_syncro]");
          trapserver_syncro($trapserver, $ch, $connect_url, $connect_api);
         }

         if ( $trapserver_active == "1" || $first_cycle == "1" ) {
          //echo message_addlog($func_role, "[trapserver_check]");
          trapserver_check($trapserver, $ch, $connect_url, $connect_api);
         }

        } else {
         $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
         echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
        }

       }

      }


        ////////////////////////
       // Router             //
      ////////////////////////

      if ( $router != "none" ) {

       curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_router");

       $pdata = array(
        'id_router' => $router,
        'api' => $connect_api
       );

       curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

       $return=curl_exec($ch);

       if(curl_errno($ch)) {
        echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
       } else {

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == "200") {

         $rou = json_decode($return, true);

         $dhcprelay_active=$rou["dhcprelay"]["active"];
         $dhcprelay_config=$rou["dhcprelay"]["config"];
         $dhcprelay_update=$rou["dhcprelay"]["update"];

         $vrdat_update=$rou["vrdat"]["update"];
         $vrdat_syncro=$rou["vrdat"]["syncro"];

         $ipfw_update=$rou["ipfw"]["update"];
         $ipfw_syncro=$rou["ipfw"]["syncro"];


         if ( $vrdat_syncro == "1" || $first_cycle == "1" ) {
          echo message_addlog($func_role, "[vrdat_syncro]");
          vrdat_syncro($router, $ch, $connect_url, $connect_api, $name_vers);
         }

         if ( $vrdat_update == "1" || $first_cycle == "1" ) {
          echo message_addlog($func_role, "[vrdat_update]");
          vrdat_update($router, $ch, $connect_url, $connect_api);
         }


         if ( $ipfw_syncro == "1" || $first_cycle == "1"  ) {
          echo message_addlog($func_role, "[ipfw_syncro]");
          ipfw_syncro($router, $ch, $connect_url, $connect_api);
         }

         if ( $ipfw_update == "1" || $first_cycle == "1" ) {
          echo message_addlog($func_role, "[ipfw_update]");
          ipfw_update($router, $ch, $connect_url, $connect_api);
         }

         if ( $dhcprelay_active == "1" ) {

          if ( $dhcprelay_config == "1" || $first_cycle == "1" ) {
           echo message_addlog($func_role, "[dhcpdelay_config]");
           dhcprelay_config($router, $ch, $connect_url, $connect_api);
          }

          if ( $dhcprelay_update == "1" || $first_cycle == "1" ) {
           echo message_addlog($func_role, "[dhcpdelay_update]");
           dhcprelay_update($router, $ch, $connect_url, $connect_api);
          }

         }

        } else {
         $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
         echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
        }

       }

      }



        ////////////////////////
       // CManager           //
      ////////////////////////

      if ( $cmanager != "none" ) {
       foreach (explode(".", $cmanager) as $id_comx_manager) {

        curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_cmanager");

         $pdata = array(
          'id_comx_manager' => $id_comx_manager,
          'api' => $connect_api
         );

        curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

        $return=curl_exec($ch);

        if(curl_errno($ch)) {
         echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
        } else {

         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         if ($httpcode == "200") {

          $cman = json_decode($return, true);
          $cmanager_ovsp_task=$cman["ovsp"]["task"];
          $cmanager_comx_task=$cman["comx"]["task"];
          $cmanager_alarm_task=$cman["alarm"]["task"];

          if ( $cmanager_ovsp_task=="1" ) {
           cmanager_ovsp_task($id_comx_manager, $ch, $connect_url, $connect_api);
          }

          if ( $cmanager_comx_task=="1" ) {
           cmanager_comx_task($id_comx_manager, $ch, $connect_url, $connect_api);
          }

          if ( $cmanager_alarm_task=="1" && get_exists_vi_cron($vi_cron,1) ) {
           alarm_task($id_comx_manager, "alarm_task", $ch, $connect_url, $connect_api);
           alarm_task($id_comx_manager, "alarm_sync", $ch, $connect_url, $connect_api);
          }

          cmanager_comx_check($id_comx_manager, $ch, $connect_url, $connect_api);
          cmanager_ovsp_check($id_comx_manager, $ch, $connect_url, $connect_api);

          alarm_check("alarm_task", $ch, $connect_url, $connect_api);
          alarm_check("alarm_sync", $ch, $connect_url, $connect_api);

         } else {
          $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
         }

        }
       }
      }

        ////////////////////////
       // GPON Server        //
      ////////////////////////
      if ( $gponserver != "none" ) {

       $a_gponserver=explode(",",$gponserver);
       gponserver_config($a_gponserver, $ch, $connect_url, $connect_api);

       foreach (explode(",", $gponserver) as $id_gponserver) {

        curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_gpon");

        $pdata = array(
         'id_gponserver' => $id_gponserver,
         'api' => $connect_api
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

        $return=curl_exec($ch);

        if(curl_errno($ch)) {
         echo system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
        } else {

         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         if ($httpcode == "200") {

          $stat=json_decode($return, true);
          $stat_gpon_sync=$stat["gpon"]["sync"];
          $stat_gpon_bi=$stat["gpon"]["bi"];
          $stat_gpon_fs=$stat["gpon"]["fs"];

          if ( $stat_gpon_fs=="1" && get_exists_vi_cron($vi_cron,30) ) {
           gponserver_task($id_gponserver, "gpon_fs", $ch, $connect_url, $connect_api);
          }

          if ( $stat_gpon_sync=="1" && get_exists_vi_cron($vi_cron,1) ) {
           gponserver_task($id_gponserver, "gpon_sync", $ch, $connect_url, $connect_api);
          }

          if ( $stat_gpon_bi=="1" && get_exists_vi_cron($vi_cron,60) ) {
           gponserver_task($id_gponserver, "gpon_bi", $ch, $connect_url, $connect_api);
          }

         } else {
          $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
         }

        }

       }

       gponserver_check("gpon_fs", $ch, $connect_url, $connect_api);
       gponserver_check("gpon_sync", $ch, $connect_url, $connect_api);
       gponserver_check("gpon_bi", $ch, $connect_url, $connect_api);

      }


        ////////////////////////
       // CMTS server        //
      ////////////////////////

      if ( $cmtsserver != "none" ) {
       foreach (explode(",", $cmtsserver) as $id_cmts_sh) {

        curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_cmts");

        $pdata = array(
         'id_cmts_sh' => $id_cmts_sh,
         'api' => $connect_api
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

        $return=curl_exec($ch);

        if(curl_errno($ch)) {
         echo system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
        } else {

         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         if ($httpcode == "200") {

          $cmts=json_decode($return, true);
          $cmts_name=$cmts["cmts"]["name"];
          $cmts_stat=$cmts["cmts"]["stat"];

          if ( get_exists_vi_cron($vi_cron,5) and $cmts_stat == "1" ) {
           cmts_task("cmts_stat", $id_cmts_sh, $cmts_name, $ch, $connect_url, $connect_api);
          }

         } else {
          $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          echo system_addlog("cmts_stat", $func_role,"1","Error [".$httpcode."] ".$last_url);
         }

        }
       }

       cmts_check($ch, $connect_url, $connect_api);

      }


        ////////////////////////
       // Statistic          //
      ////////////////////////

      if ( $statistic != "none" ) {
       foreach (explode(",", $statistic) as $id_statistic) {

       curl_setopt($ch, CURLOPT_URL, $connect_url."get_semaphores_statistic");

       $pdata = array(
        'id_statistic' => $id_statistic,
        'api' => $connect_api
       );

       curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

       $return=curl_exec($ch);

       if(curl_errno($ch)) {
        echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
       } else {

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == "200") {

         $stat=json_decode($return, true);
         $stat_comx_active=$stat["comx"]["active"];
         $stat_comx_stat=$stat["comx"]["stat"];
         $stat_docx_active=$stat["docx"]["active"];
         $stat_docx_prestat=$stat["docx"]["prestat"];
         $stat_docx_stat=$stat["docx"]["stat"];

         if ( $stat_comx_active=="1" && get_exists_vi_cron($vi_cron,1) ) {
          statistic_task($id_statistic, "comx_active", $ch, $connect_url, $connect_api);
         }
         if ( $stat_comx_stat=="1" && get_exists_vi_cron($vi_cron,5) ) {
          statistic_task($id_statistic, "comx_stat", $ch, $connect_url, $connect_api);
         }
         if ( $stat_docx_active=="1" && get_exists_vi_cron($vi_cron,1) ) {
          statistic_task($id_statistic, "docx_active", $ch, $connect_url, $connect_api);
         }
         if ( $stat_docx_prestat=="1" ) {
          statistic_task($id_statistic, "docx_prestat", $ch, $connect_url, $connect_api);
         }
         if ( $stat_docx_stat=="1" && get_exists_vi_cron($vi_cron,5) ) {
          statistic_task($id_statistic, "docx_stat", $ch, $connect_url, $connect_api);
         }

        } else {
         $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
         echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
        }

       }
      }

      statistic_check("comx_active", $ch, $connect_url, $connect_api);
      statistic_check("comx_stat", $ch, $connect_url, $connect_api);
      statistic_check("docx_active", $ch, $connect_url, $connect_api);
      statistic_check("docx_stat", $ch, $connect_url, $connect_api);

     }

        ////////////////////////
       // ERRORS manager     //
      ////////////////////////
     get_errors($config["connect"]["url"], $connect_username, $connect_password);


        ////////////////////////
       // TASKS manager      //
      ////////////////////////
     check_vivabill_tasks ($uuid, $ch, $connect_url, $connect_api);


        ////////////////////////
       // End of tasks       //
      ////////////////////////

     $first_cycle = "0"; 

     } // end task process

    } else {
     $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
     echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
    }

   }

   sleep ($current_notify_interval);

   $counter=$counter+$current_notify_interval;

   } // End cycle

  curl_close ($ch);
  echo message_addlog ($func_role, "Close process UUID=".$uuid);

 }

  // e_work -----------------------------------------

 } else {
  echo message_addlog($func_role, "PID was not found.");
 }

}

task_manager($argv[1]);

?>
