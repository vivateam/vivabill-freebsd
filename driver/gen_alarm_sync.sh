#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

  function set_task($vfile_task, $vid, $vstatus, $vexport) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"export\":\"".$vexport."\"}}"
                                              );

  }

  $export="";

  try {

   set_task ($file_task, $id, "run","");

   $file_task=$alarm_dir."alarm_sync_".$id.".task";
   $file_data=$alarm_dir."alarm_sync_".$id.".data";

   $a_alarm = file_get_contents ($file_data);
   $r_alarm = json_decode($a_alarm,true);

   foreach ($r_alarm as $config) {

    $id_boxc_alarm=$config["id_boxc_alarm"];
    $ip=$config["ip"];
    $mode=$config["mode"];
    $community=$config["community"];
    $status=$config["status"];
    $mib_ud=$config["mib_ud"];
    $mib_os=$config["mib_os"];
    $val_ud='x';
    $val_ud='x';

    $session = new SNMP(SNMP::VERSION_2c, $ip, $community, 100000, 2);
    $err = $session->getError();
    if ( $err == "" ) {

     $session->valueretrieval = SNMP_VALUE_PLAIN;
     $val_ud = $session->get($mib_ud);

     // status UP ----------------------------------------------------
     if ( $val_ud=="1" ) {

      $val_os = $session->get($mib_os);

      // STATUS UP ----------------------------
      if ( $val_os=="1" ) { $val_status="auto"; }

      // status DOWN --------------------------
      if ( $val_os == "2" ) { $val_status="down"; }

      // status TESTING -----------------------
      if ( $val_os == "3" ) { $val_status="testing"; }

      // status UNKNOWN -----------------------
      if ( $val_os == "4" ) { $val_status="unknown"; }

      // status DORMANT -----------------------
      if ( $val_os == "5" ) { $val_status="dormant"; }

      // status NotPresent --------------------
      if ( $val_os == "6" ) { $val_status="notpresent"; }

      // status LLD ---------------------------
      if ( $val_os == "7" ) { $val_status="lowerlayerdown"; }

     }

     // status CLOSE --------------------------------------------------
     if ( $val_ud == "2" ) {
      $val_status="close";
     }

     // status TEST ---------------------------------------------------
     if ( $val_ud == "3" ) {
      $val_status="test";
     }

     if( $status<>$val_status) {
      $export=$export."L~s~".$id_boxc_alarm."~s~".$val_status."~ent~";
      echo message_addlog($func_role, "Change port status from [".$status."] to [".$val_status."] on device IP ".$ip);
     }

     $session->close();

    }

   }

   set_task ($file_task, $id, "success",$export);

  } catch (Exception $e) {
   set_task ($file_task, $id, "error",$e->getMessage());
   echo system_addlog($func_role,"1", $e->getMessage());
  }

 }

 task_executant($argv[1]);

?>