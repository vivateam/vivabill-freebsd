#!/usr/local/bin/php
<?php
 function task_executant($id) {

  include ("/usr/local/etc/vivabill/config.php");
  include ("/usr/local/etc/vivabill/fn_core.php");

  $ddrt=date('Y.m.d H:i:00', time());
  $ddr=date('Y-m-d', time());
  $ddrtm=date('H:i:00', time());

  $func_role = basename(__FILE__)." ".__FUNCTION__ ;
  error_reporting(0);

  function set_task($vfile_task, $vid, $vstatus, $vexport) {
     file_put_contents($vfile_task, "{\"process\":{".
                                    "\"id\":\"".$vid."\",".
                                    "\"pid\":\"".getmypid ()."\",".
                                    "\"status\":\"".$vstatus."\",".
                                    "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                    "\"export\":\"".$vexport."\"}}"
    );
  }

  $export="";

  $file_task=$gpon_dir."gpon_sync_".$id.".task";
  $file_data=$gpon_dir."gpon_sync_".$id.".data";

  set_task ($file_task, $id, "run","");

  $array_snmp_model = array("HG8245C","HG8245D","HG8010C", "HG8010H", "HG8010B", "HG8310", "HG8311");

  $a_data = file_get_contents ($file_data);
  $data = json_decode($a_data, true);
  $ip=$data["ip"];
  $community=$data["community"];
  $snmp_model=$data["snmp_model"];

  $session = new SNMP(SNMP::VERSION_2c, $ip, $community);
  $err = $session->getError();
  if ( $err == "" ) {

   # U
   $output_walk = shell_exec("/usr/bin/snmpwalk -v 2c -c ".$community." -Ir -OqEtx ".$ip." 1.3.6.1.4.1.2011.6.128.1.1.2.48.1.2");
   $output_walk = explode(chr(10), $output_walk);
   foreach ($output_walk as $id_walk => $record_walk) {
    $valk = explode ('"',$record_walk);
    $serial = str_replace (" ","",$valk[1]);
    if ( $serial != "" ) {
     $valp = explode (".",$record_walk);
     $vals = explode (" ",$valp[11]);
     $export=$export."U~s~".$valp[10]."~s~".$vals[0]."~s~".$serial."~ent~";
#     echo message_addlog($func_role, "U~s~".$valp[10]."~s~".$vals[0]."~s~".$serial."~ent~");
    }
   }

   # P
   $session->valueretrieval = SNMP_VALUE_PLAIN;
   $r_ifname = $session->walk(".1.3.6.1.2.1.31.1.1.1.1");
   foreach($r_ifname as $ifid => $ifname) {
    $ifid = explode ("IF-MIB::ifName.", $ifid);
    if ( preg_match('#' . "GPON" . '#i', $ifname) ) {
     $ifname = str_replace(" ","", $ifname);
     $ifname = str_replace("GPON","", $ifname);
     $ifchassis = explode ("/", $ifname);
     $session->valueretrieval = SNMP_VALUE_LIBRARY;
     $export=$export."P~s~".$ifid[1]."~s~".$ifchassis[0]."~s~".$ifchassis[1]."~s~".$ifchassis[2]."~ent~";
#     echo message_addlog($func_role, "P~s~".$ifid[1]."~s~".$ifchassis[0]."~s~".$ifchassis[1]."~s~".$ifchassis[2]."~ent~");

     $output_wd = shell_exec('/usr/bin/snmpwalk -v 2c -c '.$community.' -Ir -OqEtx '.$ip.' 1.3.6.1.4.1.2011.6.128.1.1.2.43.1.3.'.$ifid[1]);
     $output_wd = explode(chr(10), $output_wd);

     foreach ($output_wd as $id_wd => $record_wd) {
      $valk = explode ('"',$record_wd);
      $cou_array = count(array_keys($valk, ""));

      if ( $cou_array > 0 ) {
       $serial_stat = str_replace (' ','',$valk[1]);
       if ( $serial_stat != '' ) {

        $valp = explode ('.',$record_wd);
        $vals = explode (' ',$valp[11]);

        $name_snmp_model='';
        $conf_stat='';
        $match_stat='';
        $lineprofile_stat='';
        $srvprofile_stat='';

        $session->valueretrieval = SNMP_VALUE_PLAIN;
        $run_stat = $session->get("1.3.6.1.4.1.2011.6.128.1.1.2.46.1.15.".$ifid[1].".".$vals[0]);

        if ( $run_stat == "1" ) {

         $conf_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.46.1.16.".$ifid[1].".".$vals[0]);
         $match_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.46.1.18.".$ifid[1].".".$vals[0]);
         $lineprofile_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.43.1.7.".$ifid[1].".".$vals[0]);
         $srvprofile_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.43.1.8.".$ifid[1].".".$vals[0]);

         $trash_snmp_model = $session->get(".1.3.6.1.4.1.2011.6.145.1.1.1.4.1.5.".$ifid[1].".".$vals[0]);
         $name_snmp_model='none';

         foreach ( $array_snmp_model as $pattern_snmp_model ) {
          if (preg_match("/".$pattern_snmp_model."/i", $trash_snmp_model)) {
           $name_snmp_model=$pattern_snmp_model;
          }

         }

        }

        $export=$export."R~s~".$ifid[1]."~s~".$vals[0]."~s~".$serial_stat."~s~".$name_snmp_model."~s~".
                $run_stat."~s~".$conf_stat."~s~".$match_stat."~s~".$lineprofile_stat."~s~".$srvprofile_stat."~ent~";
#        echo message_addlog($func_role,"R~s~".$ifid[1]."~s~".$vals[0]."~s~".$serial_stat."~s~".$name_snmp_model."~s~".
#                $run_stat."~s~".$conf_stat."~s~".$match_stat."~s~".$lineprofile_stat."~s~".$srvprofile_stat."~ent~");


       }

      }

     }

    }

   }

   # ZZZ

   $session->close();

  }

  set_task ($file_task, $id, "success",$export);


 }
 task_executant($argv[1]);
?>