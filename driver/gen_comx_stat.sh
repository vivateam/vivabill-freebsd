#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vexport) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"export\":\"".$vexport."\"}}"
                                              );

 }

  $export="";
  $file_task=$stat_dir."comx_stat_".$id.".task";
  $file_data=$stat_dir."comx_stat_".$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $config_data = json_decode($a_data, true);

  foreach ($config_data as $config) {

   $id_comx=$config["id_comx"];
   $ip_user=$config["ip_user"];
   $mib_serial=$config["mib_serial"];
   $mib_model=$config["mib_model"];
   $comx_serial=$config["comx_serial"];
   $comx_model=$config["comx_model"];
   $community=$config["community"];

   $session = new SNMP(SNMP::VERSION_2c, $ip_user, $community, 100000, 2);
   $session->valueretrieval = SNMP_VALUE_PLAIN;
   $arr_serial = explode(" ", $session->get($mib_serial));
   $arr_model = explode(" ", $session->get($mib_model));
   $err = $session->getError();

   if ( $err == "" ) {

    $name_model = $arr_model[0];
    $name_serial = $arr_serial[0];

    if ( $name_model != $comx_model || $name_serial != $comx_serial ) {
     $export=$export.$id_comx."~s~".$ip_user."~s~".$name_model."~s~".$name_serial."~ent~";
    }

    $session->close();

   }

  }

  set_task ($file_task, $id, "success",$export);

 }

 task_executant($argv[1]);

?>