#!/usr/local/bin/php
<?php

# This script accepts only one parameter - ID task
# Before run this script, system will generate file with data,
# which contains all require parameters.
#
# Before start process of namagement device, this script MUST create
# file with task extension, and keep in it information about process
# of management in JSON data with next parameters:
#    id       ID task
#    pid      PID of this script process
#    status   Task status (accepts "run", "success", or "error")
#    date     Date of change/create task
#    comment  Text information which can be sent to billing 
#             to report about process, or text error
#
# After finish of management device, script MUST set result in status section
# Notice:
#  Task with "success" status will automaticly closed by vivabill process
#  Task with "error" status will recreate task again, and report billing
#  Task with "run" status will ignored
#
# DATA SECTION contains data which required to manage device.
# Please, use lines in DATA SECTION if they necessary in CONFIG SECTION
# CONFIG SECTION contains code hardware configuration

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vcomment) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"comment\":\"".$vcomment."\"}}"
                                              );

 }

  $file_task=$comx_dir.$id.".task";
  $file_data=$comx_dir.$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $data = json_decode($a_data, true);

  # BEGIN DATA SECTION

  $id_comx=$data["id_comx"];
  $id_driver_mode=$data["model"]["id_driver_mode"];
  $model_name=$data["model"]["name"];
  $model_vlan_mib=$data["model"]["vlan_mib"];

  $access_community=$data["access"]["community"];
  $access_login=$data["access"]["login"];
  $access_password=$data["access"]["password"];
  $access_ip_cur=$data["access"]["ip_cur"];
  $access_ip_new=$data["access"]["ip_new"];

  $zone_community=$data["zone"]["community"];
  $zone_login=$data["zone"]["login"];
  $zone_password=$data["zone"]["password"];
  $zone_num_vlan_sl=$data["zone"]["num_vlan_sl"];
  $zone_num_vlan_re=$data["zone"]["num_vlan_re"];

  if ( $access_ip_new !="" ) {
   $check_ip=$access_ip_new;
  } else {
   $check_ip=$access_ip_cur;
  }

  if ( $access_community != "" ) {
   $check_community = $access_community;
  } else {
   $check_community = $zone_community;
  }

  $check_mib = "SNMPv2-MIB::sysContact.0";

  # END DATA SECTION

  # BEGIN CONFIG SECTION

  $exp_file=$tftp_config.$id.".exp";
  $tmp_file=$tftp_config.$id.".tmp";

  set_task($file_task,$id,"run","Start execute. ".$expect." -f ".$exp_file);

  $R="\033[0;31m"; $G="\033[0;32m"; $N="\033[0m";
  $log_file = $log_dir."comx_expect_".$id_comx.".log";
  file_put_contents($log_file, chr(10)."${G}[".date('Y-m-d H:i:s', time())."] [".$func_role."] [run]${N}".chr(10), FILE_APPEND | LOCK_EX);

  # Get VLANs from JSON data
  set_task($file_task,$id,"run","Get VLANs from JSON data");
  $a_vlans_bi=array();
  $a_vlans_delete=array();

  array_push($a_vlans_bi,
             $zone_num_vlan_sl,
             $zone_num_vlan_re);

  $execute_vlan_port=$data["execute"]["vlan_port"];
  foreach ( $execute_vlan_port as $row) {
   $vlan=$row["vlan"];
   array_push($a_vlans_bi,$vlan);
  }

  # Get VLANs from device
  set_task($file_task,$id,"run","Get VLANs from device");
  $session = new SNMP(SNMP::VERSION_2c, $access_ip_cur, $check_community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $a_descr = $session->walk($model_vlan_mib);
  $err = $session->getError();
  if ( $err == "" ) {
   foreach ( $a_descr as $key => $rec_descr ) {
    if (!in_array($rec_descr, $a_vlans_bi)) {
     array_push($a_vlans_delete,$rec_descr);
    }
   }
   $session->close();
  }

  if ( count($a_vlans_delete) > 0 ) {

   $expvd_file=$tftp_config."vd".$id.".exp";
   set_task($file_task,$id,"run","Delete VLANs from device ".$expect." -f ".$expvd_file);

   file_put_contents($expvd_file,"spawn /usr/bin/ssh ".$access_login."@".$access_ip_cur.chr(10), FILE_APPEND | LOCK_EX);
   file_put_contents($expvd_file,"expect \"*(yes/no)*\" {send \"yes\\r\"}".chr(10), FILE_APPEND | LOCK_EX);
   file_put_contents($expvd_file,"expect \"*assword*\" {send \"".$access_password."\\r\"}".chr(10), FILE_APPEND | LOCK_EX);
   file_put_contents($expvd_file,"expect \"*#\" {send \"configure\\r\"}".chr(10), FILE_APPEND | LOCK_EX);

   foreach ( $a_vlans_delete as $vlan_delete) {
    file_put_contents($expvd_file,"expect \"*#\" {send \"no vlan ".$vlan_delete."\\r\"}".chr(10), FILE_APPEND | LOCK_EX);
   }

   file_put_contents($expvd_file,"expect \"*#\" {send \"exit\\r\"}".chr(10), FILE_APPEND | LOCK_EX);
   file_put_contents($expvd_file,"expect \"*#\" {send \"write memory\\r\"}".chr(10), FILE_APPEND | LOCK_EX);
   file_put_contents($expvd_file,"sleep 10".chr(10), FILE_APPEND | LOCK_EX);

   echo system_exec_addlog($func_role, $expect." -f ".$expvd_file." >> ".$log_file);

  }
  echo system_exec_addlog($func_role, $expect." -f ".$exp_file." >> ".$log_file);

  set_task($file_task,$id,"run","Stop execute. ".$expect." -f ".$exp_file);
  set_task($file_task,$id,"run","Prepare to check updates by SNMP. ".$check_ip." ".$check_mib);

  $session = new SNMP(SNMP::VERSION_2c, $check_ip, $check_community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $check_id = $session->get($check_mib);
  $err = $session->getError();
  if ( $err == "" ) {

   if ( $check_id == $id ) {
    $result="success";
    if (file_exists($exp_file)) { unlink($exp_file); }
    if (file_exists($tmp_file)) { unlink($tmp_file); }
   } else {
    $err = "check_id=".$check_id.' id='.$id;
   }

  $session->close();
  } else {
   echo message_addlog($func_role, "Device COMX ID-$id_comx offline");
  }

  # END CONFIG SECTION

  if  ( $result == "success") {
   set_task ($file_task, $id, "success",$comment);
   file_put_contents($log_file, chr(10)."${G}[".date('Y-m-d H:i:s', time())."] [".$func_role."] [success]${N}".chr(10), FILE_APPEND | LOCK_EX);
  } else {
   set_task ($file_task, $id, "error", $err);
   file_put_contents($log_file, chr(10)."${R}[".date('Y-m-d H:i:s', time())."] [".$func_role."] [error]${N}".chr(10), FILE_APPEND | LOCK_EX);
  }

 }

 task_executant($argv[1]);

?>