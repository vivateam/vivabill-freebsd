#!/usr/local/bin/php
<?php
 function task_executant($id, $action) {

  include ("/usr/local/etc/vivabill/config.php");
  include ("/usr/local/etc/vivabill/fn_core.php");

  $ddrt=date('Y.m.d H:i:00', time());
  $ddr=date('Y-m-d', time());
  $ddrtm=date('H:i:00', time());

  $func_role = basename(__FILE__)." ".__FUNCTION__ ;
  error_reporting(0);

  function set_task($vfile_task, $vid, $vstatus, $vexport) {
      file_put_contents($vfile_task, "{\"process\":{".
                                     "\"id\":\"".$vid."\",".
                                     "\"pid\":\"".getmypid ()."\",".
                                     "\"status\":\"".$vstatus."\",".
                                     "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                     "\"export\":\"".$vexport."\"}}"
      );

  }

  $export="";

  if (!is_dir($docx_stat_dir.$ddr) && strlen($docx_stat_dir.$ddr)>0) { mkdir($docx_stat_dir.$ddr, 0777,true); }

  $file_task=$cmts_dir.$action."_".$id.".task";
  $file_data=$cmts_dir.$action."_".$id.".data";

  set_task ($file_task, $id, "run","");
  $a_data = file_get_contents ($file_data);
  $config = json_decode($a_data, true);

  $name_cmts=$config["name_cmts"];
  $ip=$config["ip"];
  $community=$config["community"];
  $modem_data=$config["modem_data"];
  $cmts_mibs=$config["cmts_mibs"];

  $modem_data_idd_ip  = array();
  $modem_data_ip_dsus = array();
  $modem_data_ip  = array();

  foreach ($modem_data as $row) {
   $srow=explode("-",$row);
   array_push($modem_data_idd_ip, $srow[2]."-".$srow[1]);
   array_push($modem_data_ip_dsus, $srow[1]."-".$srow[3]."-".$srow[4]);
   $modem_data_ip[$srow[1]] = $srow[0];
  }

  $session = new SNMP(SNMP::VERSION_2c, $ip, $community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;

  # Get staistic from CMTS
  $if_index = $session->walk("IF-MIB::ifIndex");
  $err = $session->getError();
  if ( $err == "" ) {
   foreach ($if_index as $key_index => $val_index) {
    $if_descr = $session->walk("IF-MIB::ifDescr.".$val_index);
    foreach ($if_descr as $key_descr => $val_descr) {
     $pattern = "/chassis (.+), slot (.+), mac (.+)/i";
     preg_match_all($pattern, $val_descr, $matches);
     $chassis=$matches[1][0];
     $slot=$matches[2][0];
     $mac=$matches[3][0];

     # DOWNSTREAM
     if ( preg_match('#' . "HFC Downstream" . '#i', $val_descr) or
          preg_match('#' . "HFC MCMTS Downstream" . '#i', $val_descr)) {

      $ds_admin_status = $session->get("IF-MIB::ifAdminStatus.".$val_index);
      if ( $ds_admin_status == "1" ) {
       $dsf = $session->get("DOCS-IF-MIB::docsIfDownChannelFrequency.".$val_index);
       $dsw = $session->get("DOCS-IF-MIB::docsIfDownChannelWidth.".$val_index);
       $dsm = $session->get("DOCS-IF-MIB::docsIfDownChannelModulation.".$val_index);
       $dsp = $session->get("DOCS-IF-MIB::docsIfDownChannelPower.".$val_index);
       $export=$export."A~s~DS~s~".$id."~s~".$val_index."~s~".$chassis."~s~".$slot."~s~".$mac.
       "~s~".$ds_admin_status."~s~0~s~".$dsf."~s~".$dsw."~s~".$dsm."~s~".$dsp."~s~0~s~0~s~0~ent~";
      }
     }

     # UPSTREAM
     if ( preg_match('#' . "HFC Logical Channel" . '#i', $val_descr) ) {

      $us_oper_status = $session->get("IF-MIB::ifOperStatus.".$val_index);
      if ( $us_oper_status == "1" ) {
       $usi = $session->get("DOCS-IF-MIB::docsIfUpChannelId.".$val_index);
       $usf = $session->get("DOCS-IF-MIB::docsIfUpChannelFrequency.".$val_index);
       $usw = $session->get("DOCS-IF-MIB::docsIfUpChannelWidth.".$val_index);
       $ust = $session->get("DOCS-IF-MIB::docsIfUpChannelType.".$val_index);
       $usn = $session->get("DOCS-IF-MIB::docsIfSigQSignalNoise.".$val_index);
       $usma = $session->get(".1.3.6.1.4.1.3493.2.4.1.1.25.1.20.".$val_index);
       $usmo = $session->get(".1.3.6.1.4.1.3493.2.4.1.1.25.1.1.".$val_index);
       $usp = $session->get("DOCS-IF-MIB::docsIfUpChannelModulationProfile.".$val_index);
       $export=$export."A~s~US~s~".$id."~s~".$val_index."~s~".$chassis."~s~".$slot."~s~".$mac.
       "~s~".$us_oper_status."~s~".$usi."~s~".$usf."~s~".$usw."~s~".$ust."~s~".$usn."~s~".$usma."~s~".$usmo."~s~".$usp."~ent~";
      }

     }

    }
   }
  }

  # Get staistic from modems
  $output = $session->walk("DOCS-IF-MIB::docsIfCmtsCmStatusIpAddress");
  $err = $session->getError();
  if ( $err == "" ) {
   foreach ($output as $key => $rec_table) {

    $a_id_docsis=explode(".",$key);
    $id_docsis=$a_id_docsis[1];

    $a_ip_modem=explode(" ",$rec_table);
    $ip_modem=$a_ip_modem[0];

    if ( $ip_modem != "0.0.0.0" ) {

     $ds = $session->get("DOCS-IF-MIB::docsIfCmtsCmStatusDownChannelIfIndex.".$id_docsis);
     $us = $session->get("DOCS-IF-MIB::docsIfCmtsCmStatusUpChannelIfIndex.".$id_docsis);

     if ( $id_docsis > 0) {
      if (!in_array($id_docsis."-".$ip_modem, $modem_data_idd_ip)) {
       $export=$export."I~s~".$id_docsis."~s~".$ip_modem."~ent~";
      }
     }

     if ( $ds > 0 && $us > 0 ) {
      if (!in_array($ip_modem."-".$ds."-".$us, $modem_data_ip_dsus)) {
       $export=$export."M~s~".$ip_modem."~s~".$ds."~s~".$us."~ent~";
      }
     }

     if (isset($modem_data_ip[$ip_modem])) {
      if ( $modem_data_ip[$ip_modem] != "" ) {
       $id_modem=$modem_data_ip[$ip_modem];
       $modem_cmts_path=$docx_stat_dir.$ddr."/".$id_modem;
       if (!is_dir($modem_cmts_path) && strlen($modem_cmts_path)>0) { mkdir($modem_cmts_path, 0777,true); }
        foreach ($cmts_mibs as $mibs) {
         $val_mibs = $session->get($mibs.".".$id_docsis);
         if ( is_numeric($val_mibs) ) {
          $val_mibs = $val_mibs/10;
          $stat_rec=$ddrtm." ".$val_mibs.chr(10);
          $modem_cmts_file=$modem_cmts_path."/".$mibs.".log";
          file_put_contents($modem_cmts_file, $stat_rec, FILE_APPEND | LOCK_EX);
         }
        }
      }
     }


    }

   }
  }

  set_task ($file_task, $id, "success",$export);

 }

 task_executant($argv[1], $argv[2]);

?>