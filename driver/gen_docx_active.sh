#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vexport) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"export\":\"".$vexport."\"}}"
                                              );

 }

  $export="";
  $file_task=$stat_dir."docx_active_".$id.".task";
  $file_data=$stat_dir."docx_active_".$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $config_data = json_decode($a_data, true);

  foreach ($config_data as $config) {

   $active=$config["active"];
   $serial=$config["serial"];
   $describe=$config["describe"];
   $id_modem=$config["id_modem"];
   $ip_modem=$config["ip_modem"];
   $mode=$config["mode"];
   $mib_describe=$config["mib_describe"];
   $community=$config["community"];

   $cur_active=get_device_online ($ip_modem, $mode, 80, $mib_describe, $community);

   if ( $cur_active != $active ) {
    $export=$export."A~s~".$id_modem."~s~".$cur_active."~ent~";
   }

   if ( $cur_active == "1" ) {
    if ( $mode == "2" ) {
     if ( $describe == "1" || $serial == "1" ) {

      $session = new SNMP(SNMP::VERSION_2c, $ip_modem, $community);
      $session->valueretrieval = SNMP_VALUE_PLAIN;

      if ( $describe == "1" ) {
       $val_describe = $session->get("SNMPv2-MIB::sysDescr.0");
       $err = $session->getError();
       if ( $err == "" ) {
         $val_describe=str_replace(">","",$val_describe);
         $val_describe=str_replace("<","",$val_describe);
         $arr_describe=explode("; ",$val_describe);
         $hw_rev= explode(": ", $arr_describe[0]);
         $vendor= explode(": ", $arr_describe[1]);
         $bootr= explode(": ", $arr_describe[2]);
         $sw_rev= explode(": ", $arr_describe[3]);
         $model= explode(": ", $arr_describe[4]);
          $export=$export."D~s~".$id_modem."~s~".$hw_rev[1]."~s~".$model[1]."~s~".$vendor[1]."~s~".$sw_rev[1]."~ent~";
       }
      }

      if ( $serial == "1" ) {
       $val_serial = $session->get("SNMPv2-SMI::mib-2.69.1.1.4.0");
       $err = $session->getError();
       if ( $err == "" ) {
        $export=$export."E~s~".$id_modem."~s~".$val_serial."~ent~";
        echo system_addlog($func_role,"0", "ID-".$id." ".$ip_modem." ".$val_serial );
       }
      }

      $session->close();

     }
    }
   }

  }

  set_task ($file_task, $id, "success",$export);
 }

 task_executant($argv[1]);

?>