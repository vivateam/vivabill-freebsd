#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");

 $ddrt=date('Y.m.d H:i:00', time());
 $ddr=date('Y-m-d', time());
 $ddrtm=date('H:i:00', time());

 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vexport) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"export\":\"".$vexport."\"}}"
                                              );

 }

  $export="";

  if (!is_dir($docx_stat_dir.$ddr) && strlen($docx_stat_dir.$ddr)>0) { mkdir($docx_stat_dir.$ddr, 0777,true); }

  $file_task=$stat_dir."docx_stat_".$id.".task";
  $file_data=$stat_dir."docx_stat_".$id.".data";
  $file_prestat=$stat_dir."docx_prestat_".$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $config_data = json_decode($a_data, true);

  $a_prestat = file_get_contents ($file_prestat);
  $config_prestat = json_decode($a_prestat, true);

  foreach ($config_data as $config) {

   $id_modem_model=$config["id_modem_model"];
   $id_modem=$config["id_modem"];
   $ip_modem=$config["ip_modem"];
   $mode=$config["mode"];
   $mib_describe=$config["mib_describe"];
   $community=$config["community"];

   $cur_active=get_device_online ($ip_modem, $mode, 80, $mib_describe, $community);

    if ( $cur_active == "1" ) {

    $modem_dir=$docx_stat_dir.$ddr."/".$id_modem;
    if (!is_dir($modem_dir) && strlen($modem_dir)>0) { mkdir($modem_dir, 0777,true); }
    $arr_mibs=$config_prestat["id_".$id_modem_model];

    $session = new SNMP(SNMP::VERSION_2c, $ip_modem, $community);

    foreach ( $arr_mibs as $line_mib => $mib ) {

     $valr_mib = $session->get($mib);
     $vale_mib = explode(" ",$valr_mib);
     $val_mib = $vale_mib[1];

     $err = $session->getError();
     if ( $err == "" ) {
      $modem_file = $modem_dir."/".$mib.".log";
      $row_stat = "$ddrtm"." "."$val_mib".chr(10);
      file_put_contents($modem_file, $row_stat, FILE_APPEND | LOCK_EX);
     }

    }

    $session->close();
   }
  }

  set_task ($file_task, $id, "success",$export);
 }

 task_executant($argv[1]);

?>