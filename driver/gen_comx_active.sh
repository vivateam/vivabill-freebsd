#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vexport) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"export\":\"".$vexport."\"}}"
                                              );

 }

  $export="";
  $file_task=$stat_dir."comx_active_".$id.".task";
  $file_data=$stat_dir."comx_active_".$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $config_data = json_decode($a_data, true);

  foreach ($config_data as $config) {

   $active=$config["active"];
   $id_comx=$config["id_comx"];
   $ip_user=$config["ip_user"];
   $mib_contact=$config["mib_contact"];
   $mode=$config["mode"];
   $community=$config["community"];
   $zone_community=$config["zone_community"];

   if ( $community == "" ) {
    $community = $zone_community;
   }

   $cur_active=get_device_online ($ip_user, $mode, 80, $mib_contact, $community);

    if ( $cur_active != $active ) {
     $export=$export.$id_comx."~s~".$cur_active."~ent~";
    }

  }

  set_task ($file_task, $id, "success",$export);

 }

 task_executant($argv[1]);

?>