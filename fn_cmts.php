<?php

function cmts_action ($id, $ch, $connect_url, $connect_api, $export) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $result="";
 curl_setopt($ch, CURLOPT_URL, $connect_url."set_cmts");

 $pdata = array(
  'id' => $id,
  'export' => $export,
  'api' => $connect_api
 );

 curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

 $return=curl_exec($ch);

 if(curl_errno($ch)) {
  echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
 } else {

  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if ($httpcode == "200") {
   $result_json = json_decode($return, true);
   $result = $result_json["result"];
  } else {
   $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
   echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
  }

 }
 return $result;
}

function cmts_task ($action, $id_cmts_sh, $cmts_name, $ch, $connect_url, $connect_api) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 $data_file=$cmts_dir.$action."_".$id_cmts_sh.".data";
 $task_file=$cmts_dir.$action."_".$id_cmts_sh.".task";

 if (!file_exists($task_file)) {

  curl_setopt($ch, CURLOPT_URL, $connect_url."get_cmts");

  $pdata = array(
   'id_cmts_sh' => $id_cmts_sh,
   'api' => $connect_api
  );

  curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

  $return=curl_exec($ch);

  if(curl_errno($ch)) {
   echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
  } else {
   $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   if ($httpcode == "200") {

    $config_data = json_decode($return,true);
    $fp = fopen($data_file, "w");
    fwrite($fp, json_encode($config_data));
    fclose($fp);

    $execute_name=$driver_dir.$cmts_name;
    $execute_command=$execute_name." ".$id_cmts_sh." ".$action." < /dev/null >> ".$log_dir."vivabill.log &";
    $return=system_exec($execute_command);

    if ( $return != "0" ) {
     echo system_addlog($func_role, $return, $execute_command);
    }

   } else {
    $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
   }

  }

 } else {
  echo message_addlog($func_role,"Task Shassis CMTS ID-".$id_cmts_sh." is exists");
 }

}


function cmts_check ($action, $ch, $connect_url, $connect_api) {
 include("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 foreach (glob($cmts_dir."*.task") as $filename) {
  $data = file_get_contents ($filename);
  $config_json = json_decode($data, true);

  $id=$config_json["process"]["id"];
  $pid=$config_json["process"]["pid"];
  $status=$config_json["process"]["status"];

  $file_task=$cmts_dir.$action."_".$id.".task";
  $file_data=$cmts_dir.$action."_".$id.".data";

  if ( $status == "run") {

  }

  if ( $status == "success") {

   $export=$config_json["process"]["export"];
   $arr_export=explode("~ent~",$export);
   $cou_export = count($arr_export) - 1;
   if ( cmts_action($id, $ch, $connect_url, $connect_api, $export) == "success") {
    if ( $cou_export > 0 ) {
     echo system_addlog($func_role, "0", "Task Chassis CMTS ID-".$id." - changed ".$cou_export." rows");
    }
   } else {
    echo system_addlog($func_role, "1", "Task Chassis CMTS ID-".$id." - error");
   }

   if (file_exists($file_task)) { unlink($file_task); }
   if (file_exists($file_data)) { unlink($file_data); }

  }

 }

}

?>
